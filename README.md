---
license: apache-2.0
language:
- en
thumbnail: https://h2o.ai/etc.clientlibs/h2o/clientlibs/clientlib-site/resources/images/favicon.ico
tags:
- gpt
- llm
- large language model
- open-source
---
# h2oGPT Data Card
## Summary

H2O.ai's `h2ogpt-oig-instruct-cleaned-v3` is an open-source instruct-type dataset for fine-tuning of large language models, licensed for commercial use.

- Number of rows: `302276`
- Number of columns: `2`
- Column names: `['input', 'source']`

## Source


- [Original LAION OIG Dataset](https://github.com/LAION-AI/Open-Instruction-Generalist)
- [LAION OIG data detoxed and filtered down by scripts in h2oGPT repository](https://github.com/h2oai/h2ogpt/blob/bfc3778c8db938761ce2093351bf2bf82159291e/create_data.py)

